package com.absoluteapps.arthurl.mobooru;

public class Data {
    public String thumbImgUrl = "";
    public String imgUrl = "";
    public String redditSrc = "";
    public String title = "";
    public String series = "";
    public String ogSrc = "";
    public String score = "";
    public int width = 0;
    public int height = 0;
    public double rat = 0.0;
    public boolean nsfw = false;
    public Data() {}
}
